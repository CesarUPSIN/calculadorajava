package com.example.calculadorajav;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMultiplicar;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;

    private EditText txtNum1;
    private EditText txtNum2;
    private TextView lblResultado;
    private Calculadora calculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSumar();
            }
        });
        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRestar();
            }
        });
        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnMultiplicar();
            }
        });
        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDividir();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regresar();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });
    }

    private void limpiar() {
        txtNum1.setText("");
        txtNum2.setText("");
        lblResultado.setText("");
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("¿Deseas regresar?");
        confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> { }).show();
    }

    private void iniciarComponentes() {
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnMultiplicar = findViewById(R.id.btnMultiplicar);
        btnDividir = findViewById(R.id.btnDividir);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        lblResultado = findViewById(R.id.lblResultado);
        calculadora = new Calculadora(0, 0);
    }

    private void btnSumar() {
        if(validar() != -1) {
            calculadora.setNum1(Integer.parseInt(txtNum1.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtNum2.getText().toString()));
            lblResultado.setText(String.valueOf(calculadora.sumar()));
        }
    }

    private void btnRestar() {
        if(validar() != -1) {
            calculadora.setNum1(Integer.parseInt(txtNum1.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtNum2.getText().toString()));
            lblResultado.setText(String.valueOf(calculadora.restar()));
        }
    }

    private void btnMultiplicar() {
        if(validar() != -1) {
            calculadora.setNum1(Integer.parseInt(txtNum1.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtNum2.getText().toString()));
            lblResultado.setText(String.valueOf(calculadora.multiplicar()));
        }
    }

    private void btnDividir() {
        if(validar() != -1) {
            calculadora.setNum1(Integer.parseInt(txtNum1.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtNum2.getText().toString()));
            lblResultado.setText(String.valueOf(calculadora.dividir()));
        }
    }

    private int validar() {
        if(txtNum1.getText().toString().equals("") || txtNum2.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Ingresar datos", Toast.LENGTH_LONG).show();
            return -1;
        }

        return 0;
    }
}