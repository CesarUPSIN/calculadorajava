package com.example.calculadorajav;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnIngresar;
    private Button btnCerrar;
    private EditText txtUsuario;
    private EditText txtContrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciarComponentes();
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingresar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {finish();}
        });
    }

    private void iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIngresar);
        btnCerrar = findViewById(R.id.btnCerrar);
        txtContrasena = findViewById(R.id.txtContrasena);
        txtUsuario = findViewById(R.id.txtUsuario);
    }

    private void ingresar() {
        // Asignar los Strings que se declararon en R.Value
        String strUsuario = getResources().getString(R.string.usuario);
        String strContrasena = getResources().getString(R.string.contrasena);

        if (txtUsuario.getText().toString().equals(strUsuario) && txtContrasena.getText().toString().equals(strContrasena)) {
            // Hacer el paquete de datos que se van a enviar
            Bundle bundle = new Bundle();
            bundle.putString("usuario", strUsuario);

            Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Usuario o Contraseña no válidos",
                    Toast.LENGTH_LONG).show();
        }
    }
}
